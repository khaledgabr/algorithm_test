void al ()
{

  move_seq ();

  nowYaw = 0;

  targetYaw = 0;


  nowYaw = ypr[0] * 180 / M_PI;
  nowYaw = nowYaw - pre_nowYaw;

  xDistance =  xFin - xInt;
  yDistance =  yFin - yInt;
  movingDistance = abs(countTicks/2) * 0.279;

  /*
    Serial.print(movingDistance);
    Serial.print("\t");
    Serial.println (nowYaw);
  */
  //************************************************************
  //************************************************************
  //************************************************************


  if (xDistance > 0 && yDistance >= 0)
  {
    th = (abs(yDistance) / abs(xDistance)) ;
    newMoveingYaw = atan(th);
    newMoveingYaw = newMoveingYaw * 57.295;  // in degree
    newMoveingYaw = 90 - newMoveingYaw;
  }
  else if (xDistance < 0 && yDistance >= 0)
  {
    th = (abs(yDistance) / abs(xDistance)) ;
    newMoveingYaw = atan(th);
    newMoveingYaw = newMoveingYaw * 57.295;  // in degree
    newMoveingYaw =  newMoveingYaw - 90;
  }
  else if (xDistance < 0 && yDistance < 0)
  {
    th = (abs(yDistance) / abs(xDistance)) ;
    newMoveingYaw = atan(th);
    newMoveingYaw = newMoveingYaw * 57.295;  // in degree
    newMoveingYaw = - 90 - newMoveingYaw;
  }
  else if (xDistance > 0 && yDistance < 0)
  {
    th = (abs(yDistance) / abs(xDistance)) ;
    newMoveingYaw = atan(th);
    newMoveingYaw = newMoveingYaw * 57.295;  // in degree
    newMoveingYaw = 90 + newMoveingYaw;
  }
  else if (xDistance == 0 && yDistance < 0)
  {
    newMoveingYaw = 180;
  }
  else if (xDistance == 0 && yDistance == 0)
  {
    newMoveingYaw = 0;
  }


  //************************************************************
  //************************************************************
  //************************************************************



  if (newMoveingYaw > (nowYaw + 3)  && !startStrightMoveFlag)
  {
    motor1_RPM_d = -30;
    motor2_RPM_d = +30;

    waitAfterTurnFlag = true ;
    finshTurnTime = millis();
  }
  else if ((newMoveingYaw + 3) < (nowYaw) && !startStrightMoveFlag )
  {
    motor1_RPM_d = +30;
    motor2_RPM_d = -30;
    waitAfterTurnFlag = true ;
    finshTurnTime = millis();
  }
  else
  {
    if (waitAfterTurnFlag)
    {
      motor1_RPM_d = 0;
      motor2_RPM_d = 0;

      startStrightMoveFlag = true ;
      if (millis() > finshTurnTime + 500)
      {
        waitAfterTurnFlag = false ;
        startCountTicks = true ;
        striaghtDistance = sqrt (pow (xDistance, 2) + pow (yDistance, 2));
      }
    }
    else
    {
      startCountTicks = true ;
      striaghtDistance = sqrt (pow (xDistance, 2) + pow (yDistance, 2));
      if (movingDistance < striaghtDistance)
      {
        startStrightMoveFlag = true ;
        if (newMoveingYaw > (nowYaw + 0) && !waitAfterTurnFlag)
        {
          motor1_RPM_d = -65;
          motor2_RPM_d = -55;
        }
        else if ((newMoveingYaw + 0) < (nowYaw) && !waitAfterTurnFlag)
        {
          motor1_RPM_d = -55;
          motor2_RPM_d = -65;
        }
        else
        {
          motor1_RPM_d = -60;
          motor2_RPM_d = -60;
        }

      }
      else
      {
        motor1_RPM_d = 0;
        motor2_RPM_d = 0;
        xInt = xFin;
        yInt = yFin;
        move_No ++;
        startCountTicks = false ;
        startStrightMoveFlag = false ;
      }
    }
  }

  //************************************************************
  //************************************************************
  //************************************************************

}

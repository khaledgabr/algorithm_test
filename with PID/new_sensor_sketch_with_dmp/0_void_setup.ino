//Setup serial, encoders, ultrasonic, MPU6050 and Reset functions
void setup()
{

  //Init Serial port with 115200 baud rate
  Serial.begin(115200);

  //Setup Encoders
  SetupEncoders();
  //Setup Motors
  SetupMotors();
  while (0)
  {
    digitalWrite(A_1, 0);
    digitalWrite(B_1, 1);
    analogWrite(PWM_1, 100);
  }
  //Setup Ultrasonic
  //SetupUltrasonic();
  //Setup MPU 6050
  Setup_MPU6050();
  delay(300);


  speed_control_setup ();

  nowYaw = 0;
  pre_nowYaw = 5;

  while (1)
  {
    Update_MPU6050();
    nowYaw = ypr[0] * 180 / M_PI;

    currentMillisT1 = millis ();
    if (currentMillisT1 - previousMillisT1 > 300 )
    {
      previousMillisT1 = currentMillisT1 ;

      pre_nowYaw = nowYaw;

      currentMillisT2 = millis ();
      previousMillisT2 = currentMillisT2 ;
    }


    currentMillisT2 = millis ();
    if (currentMillisT2 - previousMillisT2 > 200 )
    {
      previousMillisT2 = currentMillisT2 ;
      if (pre_nowYaw == nowYaw)
      {
        break;
      }
    }

    Serial.print (nowYaw, 3.f);
    Serial.print ("\t");
    Serial.println (pre_nowYaw, 3.f);
  }
  
}

void speed_control_setup ()
{
  PID1.SetMode(AUTOMATIC);              // PID1 Setup
  PID1.SetOutputLimits(right_motor_pwm_min , right_motor_pwm_max);
  PID1.SetSampleTime(interval);

  PID2.SetMode(AUTOMATIC);              // PID2 Setup
  PID2.SetOutputLimits(lift_motor_pwm_min , lift_motor_pwm_max);
  PID2.SetSampleTime(interval);

}




void cal_rpm ()
{
  currentMillis = millis();
  if (currentMillis - previousMillis > RPM_TEST_SAMPLE_TIME)
  {
    previousMillis = currentMillis ;

    //*******************************************************************
    motor1_RPM_a = ( pulseCounter_1 * secToMin) / 90;
    motor2_RPM_a = ( pulseCounter_2 * secToMin) / 90;
    //********************************************************************

    pulseCounter_1 = 0;
    pulseCounter_2 = 0;
  }
}


void set_rpm ()
{
  currentMillisT3 = millis();
  if (currentMillisT3 - previousMillisT3 >= interval)
  {
    previousMillisT3 = currentMillisT3;

    //***********************************************************************

    Setpoint2 = motor2_RPM_d;
    Input2 = motor2_RPM_a;
    PID2.Compute();

    Setpoint1 = motor1_RPM_d;
    Input1 = motor1_RPM_a;
    PID1.Compute();



    if (motor2_RPM_d == 0)
    {
      digitalWrite(A_2, HIGH);
      digitalWrite(B_2, HIGH);
      analogWrite(PWM_2, 255);
      Setpoint2 = 0;
      Input2 = 0;
      Output2 = 0;
    }

    if (motor1_RPM_d == 0)
    {
      digitalWrite(A_1, HIGH);
      digitalWrite(B_1, HIGH);
      analogWrite(PWM_1, 255);
      Setpoint1 = 0;
      Input1 = 0;
      Output1 = 0;
    }

    Serial.print(newMoveingYaw);
    Serial.print("\t");
    Serial.print(movingDistance);
    Serial.print("\t");
    Serial.print (nowYaw);
    Serial.print("\t");
    Serial.print(motor1_RPM_a);
    Serial.print("\t");
    Serial.print(motor1_RPM_d);
    Serial.print("\t");
    Serial.print(Output1);
    Serial.print("\t");
    Serial.print(motor2_RPM_a);
    Serial.print("\t");
    Serial.print(motor2_RPM_d);
    Serial.print("\t");
    Serial.print(Output2);
    Serial.print("\t");
    Serial.print("\n");

    //***********************************************************************

    motorsControl();
  }
}









void motorsControl()
{
  //*********************************************
  //*********************************************
  //*********************************************
  if (Output1 > 0 )
  {
    digitalWrite(A_1, 0);
    digitalWrite(B_1, 1);
    analogWrite(PWM_1, Output1);
  }
  else if (Output1 < 0)
  {

    Output1a = abs (Output1);

    digitalWrite(A_1, 1);
    digitalWrite(B_1, 0);
    analogWrite(PWM_1, Output1a);
  }
  //*********************************************
  //*********************************************
  //*********************************************
  if (Output2 > 0 )
  {
    digitalWrite(A_2, 0);
    digitalWrite(B_2, 1);
    analogWrite(PWM_2, Output2);
  }
  else if (Output2 < 0)
  {
    Output2a = abs (Output2);
    digitalWrite(A_2, 1);
    digitalWrite(B_2, 0);
    analogWrite(PWM_2, Output2a);
    //*********************************************
    //*********************************************
    //*********************************************
  }
}

void start_testing_time(byte flagNum, char m)
{
  if (m == 'M')
  {
    startTimeTesting[flagNum] = millis();
  }
  else if (m == 'm')
  {
    startTimeTesting[flagNum] = micros();
  }
  else
  {
    Serial.println ("error");
  }
}

void stop_testing_time(byte flagNum , char m)
{
  if (m == 'M')
  {
    stopTimeTesting[flagNum] = millis();
    timeTestingPeriod[flagNum] = stopTimeTesting[flagNum] - startTimeTesting[flagNum] ;
    Serial.println (timeTestingPeriod[flagNum]);
  }
  else if (m == 'm')
  {
    stopTimeTesting[flagNum] = micros();
    timeTestingPeriod[flagNum] = stopTimeTesting[flagNum] - startTimeTesting[flagNum] ;
    Serial.println (timeTestingPeriod[flagNum]);
  }
  else
  {
    Serial.println ("error");
  }
}

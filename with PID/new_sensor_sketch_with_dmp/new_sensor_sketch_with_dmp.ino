/*
  #  Chefbot_ROS_Interface.ino
  #
  #  Copyright 2015 Lentin Joseph <qboticslabs@gmail.com>
  #  Website : www.qboticslabs.com , www.lentinjoseph.com
  #  This program is free software; you can redistribute it and/or modify
  #  it under the terms of the GNU General Public License as published by
  #  the Free Software Foundation; either version 2 of the License, or
  #  (at your option) any later version.
  #
  #  This program is distributed in the hope that it will be useful,
  #  but WITHOUT ANY WARRANTY; without even the implied warranty of
  #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  #  GNU General Public License for more details.
  #
  #  You should have received a copy of the GNU General Public License
  #  along with this program; if not, write to the Free Software
  #  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  #  MA 02110-1301, USA.
  #
  #  Some of the portion is adapted from I2C lib example code for MPU 6050
*/


//MPU 6050 Interfacing libraries

//Library to communicate with I2C devices
#include "Wire.h"
//I2C communication library for MPU6050
#include "I2Cdev.h"
//MPU6050 interfacing library
#include "MPU6050_6Axis_MotionApps20.h"
//Processing incoming serial data
#include <Messenger.h>
//Contain definition of maximum limits of various data type
#include <limits.h>

#include <math.h>
//Creating MPU6050 Object
MPU6050 accelgyro(0x68);
//Messenger object
Messenger Messenger_Handler = Messenger();

///////////////////////////////////////////////////////////////////////////////////////
//DMP options
//Set true if DMP init was successful
bool dmpReady = false;
//Holds actual interrupt status byte from MPU
uint8_t mpuIntStatus;
//return status after each device operation
uint8_t devStatus;
//Expected DMP paclet size
uint16_t packetSize;
//count of all bytes currently in FIFO
uint16_t fifoCount;
//FIFO storate buffer
uint8_t fifoBuffer[64];


//#define OUTPUT_READABLE_QUATERNION
#define OUTPUT_READABLE_YAWPITCHROLL
//#define OUTPUT_READABLE_EULER
//#define OUTPUT_READABLE_REALACCEL
//#define OUTPUT_READABLE_WORLDACCEL
//#define OUTPUT_TEAPOT
////////////////////////////////////////////////////////////////////////////////////////////////

//orientation/motion vars
Quaternion q;
VectorInt16 aa;
VectorInt16 aaReal;
VectorInt16 aaWorld;
VectorFloat gravity;

float euler[3];
float ypr[3];



// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
  mpuInterrupt = true;
}


////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
//Encoder pins definition

// Left encoder

#define Left_Encoder_PinA 3
#define Left_Encoder_PinB 10

volatile long Left_Encoder_Ticks = 0;
volatile bool LeftEncoderBSet;

//Right Encoder

#define Right_Encoder_PinA 2
#define Right_Encoder_PinB 9
volatile long Right_Encoder_Ticks = 0;
volatile bool RightEncoderBSet;

/////////////////////////////////////////////////////////////////
//Motor Pin definition
//Left Motor pins

#define A_2 11
#define B_2 4
#define PWM_2 6




//Right Motor
#define A_1 8
#define B_1 12
#define PWM_1 7




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Ultrasonic pins definition
const int echo = 9, Trig = 10;
long duration, cm;

#define PUSH2 19

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Battery level monitor for future upgrade
#define BATTERY_SENSE_PIN PC_4

float battery_level = 12;

//Reset pin for resetting Tiva C, if this PIN set high, Tiva C will reset

#define RESET_PIN PB_2

/////////////////////////////////////////////////////////////////////////////////////////
//Time  update variables

unsigned long LastUpdateMicrosecs = 0;
unsigned long LastUpdateMillisecs = 0;
unsigned long CurrentMicrosecs = 0;
unsigned long MicrosecsSinceLastUpdate = 0;
float SecondsSinceLastUpdate = 0;

///////////////////////////////////////////////////////////////////////////////////////
//Motor speed from PC
//Motor left and right speed
float motor_left_speed = 0;
float motor_right_speed = 0;
/////////////////////////////////////////////////////////////////




int xInt = 0;
int yInt = 0;
float nowYaw = 0;

int xFin = 0;
int yFin = 0 ;
float targetYaw = 0;

double newMoveingYaw ;
long striaghtDistance = 0;
double movingDistance ;
double xDistance ;
double yDistance ;


double th ;
float pre_nowYaw;


volatile boolean startStrightMoveFlag = false ;
volatile boolean waitAfterTurnFlag = false ;
unsigned long finshTurnTime = 0;


volatile boolean startCountTicks = false ;
volatile long countTicks = 0;

//*********************************************************************
unsigned long currentMillisT1 = 0;
unsigned long previousMillisT1 = 0;

unsigned long currentMillisT2 = 0;
unsigned long previousMillisT2 = 0;


unsigned long currentMillisT4 = 0;
unsigned long previousMillisT4 = 0;

unsigned long currentMillisT5 = 0;
unsigned long previousMillisT5 = 0;
//*********************************************************************




byte move_No = 0 ;



//**********************************************************************************************
#include <PID_v1.h>

double Pk1      = 1;
double Ik1      = 20;
double Dk1      = 0;


double Pk2      = 1;
double Ik2      = 20;
double Dk2      = 0;


long interval   = 10;

volatile double  Setpoint1, Input1, Output1, Output1a;                       // PID variables
PID PID1(&Input1, &Output1, &Setpoint1, Pk1, Ik1 , Dk1, DIRECT);    // PID Setup

volatile double  Setpoint2, Input2, Output2, Output2a;                       // PID variables
PID PID2(&Input2, &Output2, &Setpoint2, Pk2, Ik2, Dk2, DIRECT);    // PID Setup

unsigned long currentMillisT3 = 0;
unsigned long previousMillisT3 = 0;


int right_motor_rpm_max = 70;
int right_motor_rpm_min = -70;
int lift_motor_rpm_max = 70;
int lift_motor_rpm_min = -70;


int right_motor_pwm_max = 250;
int right_motor_pwm_min = -250;
int lift_motor_pwm_max =  250;
int lift_motor_pwm_min =  -250;







volatile long pulseCounter_1 = 0;
volatile long pulseCounter_2 = 0;

volatile boolean motor1_dir_read = true ; //true -> cw false -> ccw
volatile boolean motor2_dir_read = true ;




unsigned long currentMillis = 0;
unsigned long previousMillis = 0;

volatile int long motor1_RPM_a = 0;
volatile int long motor2_RPM_a = 0;

volatile int  motor1_RPM_d = 0;
volatile int  motor2_RPM_d = 0;

byte M1_PWM_D = 0;
byte M2_PWM_D = 0;

#define RPM_TEST_SAMPLE_TIME       100      //each 100 millie second
long secToMin = 60000 / RPM_TEST_SAMPLE_TIME;

//**********************************************************************************************







//****************************************
unsigned int startTimeTesting   [5];
unsigned int stopTimeTesting    [5];
unsigned int timeTestingPeriod  [5];
//****************************************

volatile long startLoopTime = 0 ;

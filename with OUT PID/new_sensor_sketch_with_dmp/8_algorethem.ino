void al ()
{

move_seq ();

  nowYaw = 0;


  targetYaw = 0;


  nowYaw = ypr[0] * 180 / M_PI;
  nowYaw = nowYaw - pre_nowYaw;

  xDistance =  xFin - xInt;
  yDistance =  yFin - yInt;
  movingDistance = abs(countTicks/2) * 0.279;


  Serial.print(movingDistance);
  Serial.print("\t");
  Serial.println (nowYaw);

  //************************************************************
  //************************************************************
  //************************************************************
  if (xDistance > 0 && yDistance >= 0)
  {
    th = (abs(yDistance) / abs(xDistance)) ;
    newMoveingYaw = atan(th);
    newMoveingYaw = newMoveingYaw * 57.295;  // in degree
    newMoveingYaw = 90 - newMoveingYaw;

  }
  else if (xDistance < 0 && yDistance >= 0)
  {
    th = (abs(yDistance) / abs(xDistance)) ;
    newMoveingYaw = atan(th);
    newMoveingYaw = newMoveingYaw * 57.295;  // in degree
    newMoveingYaw =  newMoveingYaw - 90;
  }
  else if (xDistance < 0 && yDistance < 0)
  {
    th = (abs(yDistance) / abs(xDistance)) ;
    newMoveingYaw = atan(th);
    newMoveingYaw = newMoveingYaw * 57.295;  // in degree
    newMoveingYaw = - 90 - newMoveingYaw;
  }
  else if (xDistance > 0 && yDistance < 0)
  {
    th = (abs(yDistance) / abs(xDistance)) ;
    newMoveingYaw = atan(th);
    newMoveingYaw = newMoveingYaw * 57.295;  // in degree
    newMoveingYaw = 90 + newMoveingYaw;

  }
  else if (xDistance == 0 && yDistance < 0)
  {
    newMoveingYaw = 180;
  }
  else if (xDistance == 0 && yDistance == 0)
  {
    newMoveingYaw = 0;
  }

  //************************************************************
  //************************************************************
  //************************************************************


  if (newMoveingYaw > (nowYaw + 2))
  {
    digitalWrite(A_1, 1);
    digitalWrite(B_1, 0);
    analogWrite(PWM_1, 150);

    digitalWrite(A_2, 0);
    digitalWrite(B_2, 1);
    analogWrite(PWM_2, 150);

  }
  else if ((newMoveingYaw + 2) < (nowYaw))
  {
    digitalWrite(A_1, 0);
    digitalWrite(B_1, 1);
    analogWrite(PWM_1, 150);

    digitalWrite(A_2, 1);
    digitalWrite(B_2, 0);
    analogWrite(PWM_2, 150);

  }
  else /*if (newMoveingYaw == nowYaw)*/
  {
    startCountTicks = true ;
    striaghtDistance = sqrt (pow (xDistance, 2) + pow (yDistance, 2));
    if (movingDistance < striaghtDistance)
    {
      digitalWrite(A_1, 1);
      digitalWrite(B_1, 0);
      analogWrite(PWM_1, 150);

      digitalWrite(A_2, 1);
      digitalWrite(B_2, 0);
      analogWrite(PWM_2, 150);
    }
    else
    {
      //digitalWrite(A_1, HIGH);
      //digitalWrite(B_1, LOW);
      analogWrite(PWM_1, 0);

      //digitalWrite(A_2, HIGH);
      //digitalWrite(B_2, LOW);
      analogWrite(PWM_2, 0);
      xInt = xFin;
      yInt = yFin;
      move_No ++;
      startCountTicks = false ;
    }
  }
  //************************************************************
  //************************************************************
  //************************************************************
}
